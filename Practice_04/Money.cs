﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Practice_04
{
    public class Money
    {
        private int _denomination; // номинал купюр
        private int _numberOfNotes; // количество купюр
        public Money(int denomination, int numberOfNotes) // конструктор
        {
            _denomination = denomination;
            _numberOfNotes = numberOfNotes;
        }
        // Свойства
        public int Denomination
        {
            get
            {
                return _denomination;
            }
            set
            {
                _denomination = value;
            }
        }
        public int NumberOfNotes
        {
            get
            {
                return _numberOfNotes;
            }
            set
            {
                _numberOfNotes = value;
            }
        }
        public int AmountOfMoney
        {
            get
            {
                return _denomination * _numberOfNotes;
            }
        }
        // Вывод номинала и количества купюр
        public void Print()
        {
            Console.WriteLine($"{_numberOfNotes} купюр номиналом {_denomination}");
            Console.WriteLine("---------------");
        }
        // Определение возможна ли покупка
        public void PurchaseBudget(string valuePurchaseAmount)
        {
            int purchaseAmount;
            if (int.TryParse(valuePurchaseAmount, out purchaseAmount))
            {
                if (purchaseAmount <= (AmountOfMoney))
                {
                    Console.WriteLine("У Вас достаточно средств для покупки!");
                    Console.WriteLine("---------------");
                }
                else
                {
                    Console.WriteLine("К сожалению средств не достаточно!");
                    Console.WriteLine("---------------");
                }
            }
            else
            {
                Console.WriteLine("Неверный ввод");
            }
            
        }
        // Определение количества товара по определенной цене
        public void QuantityOfGoods(string valueCost)
        {
            int cost;
            if (int.TryParse(valueCost, out cost))
            {
                if (cost > 0)
                {
                    Console.WriteLine($"Возможно купить {AmountOfMoney / cost} шт.");
                    Console.WriteLine("---------------");
                }
                else
                {
                    Console.WriteLine("Некорректная цена");
                }
            }
            else
            {
                Console.WriteLine("Неверный ввод");
            }
        }
    }
}
