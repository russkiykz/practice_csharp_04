﻿using System;

namespace Practice_04
{
    class Program
    {
        static void Main(string[] args)
        {
            Money money = new Money(500, 10);
            money.Print();
            Console.WriteLine("Введите сумму покупки: ");
            money.PurchaseBudget(Console.ReadLine());
            Console.WriteLine("Введите цену товара: ");
            money.QuantityOfGoods(Console.ReadLine());
        }
    }
}
